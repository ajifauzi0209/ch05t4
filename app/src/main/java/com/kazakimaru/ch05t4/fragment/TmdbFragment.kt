package com.kazakimaru.ch05t4.fragment

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.Snackbar
import com.kazakimaru.ch05t4.BuildConfig
import com.kazakimaru.ch05t4.R
import com.kazakimaru.ch05t4.databinding.FragmentTmdbBinding
import com.kazakimaru.ch05t4.helper.viewModelsFactory
import com.kazakimaru.ch05t4.service.TMDBApiService
import com.kazakimaru.ch05t4.service.TMDBClient
import com.kazakimaru.ch05t4.viewmodel.TMDBViewModel


class TmdbFragment : Fragment() {

    private var _binding : FragmentTmdbBinding? = null
    private val binding get() = _binding!!

    private val apiService : TMDBApiService by lazy { TMDBClient.instance }
    private val viewModel : TMDBViewModel by viewModelsFactory { TMDBViewModel(apiService) }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        _binding = FragmentTmdbBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        viewModel.getAllMovie()
        observerData()
    }

    private fun observerData() {
        viewModel.dataSuccess.observe(viewLifecycleOwner) {
            Snackbar.make(binding.root, it.results[0].title, Snackbar.LENGTH_SHORT).show()
            binding.textPercobaan.text = it.results[0].title
            val urlImage = "${BuildConfig.BASE_URL_IMAGE}${it.results[0].posterPath}"
            Glide.with(requireContext())
                .load(urlImage)
                .into(binding.imgMovie)
        }

        viewModel.dataError.observe(viewLifecycleOwner) {
            Snackbar.make(binding.root, it, Snackbar.LENGTH_SHORT).show()
        }
    }

}