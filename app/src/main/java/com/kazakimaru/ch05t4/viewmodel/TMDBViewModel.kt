package com.kazakimaru.ch05t4.viewmodel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.kazakimaru.ch05t4.BuildConfig
import com.kazakimaru.ch05t4.model.MovieResponse
import com.kazakimaru.ch05t4.service.TMDBApiService
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class TMDBViewModel(private val apiService: TMDBApiService) : ViewModel() {

    // digunakan untuk assign value , hanya didalam viewmodel
    private val _dataSuccess = MutableLiveData<MovieResponse>()
    // digunakan untuk observe di activity/fragment
    val dataSuccess  : LiveData<MovieResponse> get() = _dataSuccess

    private val _dataError = MutableLiveData<String>()
    val dataError  : LiveData<String> get() = _dataError

    fun getAllMovie() {
        apiService.getAllMovie(BuildConfig.API_KEY)
            .enqueue(object : Callback<MovieResponse> {
                override fun onResponse(
                    call: Call<MovieResponse>,
                    response: Response<MovieResponse>
                ) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            _dataSuccess.postValue(response.body())
                        } else {
                            _dataError.postValue("data kosong")
                        }
                    } else {
                        _dataError.postValue("data gagal diambil")
                    }
                    Log.e("onResponse", "test")
                }

                override fun onFailure(call: Call<MovieResponse>, t: Throwable) {
                    _dataError.postValue("Server bermasalah")
                    Log.e("onFailure", "test")
                }

            })
    }
}
