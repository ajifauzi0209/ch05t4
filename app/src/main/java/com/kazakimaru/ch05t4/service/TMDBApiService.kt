package com.kazakimaru.ch05t4.service

import com.kazakimaru.ch05t4.model.MovieResponse
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface TMDBApiService {

    @GET("movie/popular")
    fun getAllMovie(@Query("api_key") key: String) : Call<MovieResponse>
}
